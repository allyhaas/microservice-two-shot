from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO
from .encoders import HatEncoder


#GET WORKS, POST WORKS
@require_http_methods(["GET","POST"])
def hat_list(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hat = Hat.objects.all()
        else:
            hat = Hat.objects.all()
        return JsonResponse(
            {"hat" : hat},
            encoder=HatEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            location_vo_id = content["location"]
            location = LocationVO.objects.get(import_href=location_vo_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"Message":"Invalid Locaiton ID"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )

##DELETE WORKS, GET WORKS
@require_http_methods(["DELETE", "GET", "PUT"])
def hat_show(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        hat = Hat.objects.get(id=id)
        hat.delete()
        return JsonResponse({"deleted": "OK" })
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_vo_id = content["location"]
                location = LocaitonVO.objects.get(import_href=location_vo_id)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message":"That's not a valid Location ID."},
                status = 400,
            )
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False
        )
