from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True, blank=True)
    closet_name = models.CharField(max_length=100)

    def __str__(self):
        return self.closet_name

class Hat(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    fabric = models.CharField(max_length=100, null=True, blank=True)
    color = models.CharField(max_length=100, null=True, blank=True)
    pic_url = models.URLField(max_length=200, null=True, blank=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete= models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("hat_list", kwargs={"pk": self.pk})
