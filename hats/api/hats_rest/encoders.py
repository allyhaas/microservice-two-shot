from common.json import ModelEncoder
from .models import Hat, LocationVO

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href","closet_name","id"]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "color",
        "pic_url",
        "id"
    ]
    encoders = {
        "location":LocationVODetailEncoder(),
    }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "color",
        "pic_url",
        "location",
    ]
    encoders = {
        "location" : LocationVODetailEncoder()
    }
