from django.urls import path
from .views import hat_list, hat_show

urlpatterns = [
    path("hats/", hat_list, name="hat_list"),
    path("hats/<int:id>/", hat_show, name="hat_show")
]
