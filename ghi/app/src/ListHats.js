import { useEffect, useState } from "react";

function ListHats() {
  const [hats, setHats] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/hats/");

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  };

  useEffect(()=>{
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Hat</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {
        hats.map(hat => {
          return (
            <tr key={hat.name}>
              <td>{hat.fabric}</td>
              <td>{hat.color}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}


export default ListHats;
