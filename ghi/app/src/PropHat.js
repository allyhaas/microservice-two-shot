import React, {useState} from 'react';

function PropHats(props) {
    const [hat, setHat] = useState(props.name);

    const clickHandler = () => {
        setHat("Updated!");
    };

    return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Hat</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {
        hats.map(hat => {
          return (
            <tr key={prop.name}>
              <td>{prop.fabric}</td>
              <td>{prop.color}</td>
              <td>{prop.pic_url}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
        </div>
    );
}



export default ExpenseItem
