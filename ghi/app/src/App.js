import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ListHats from './ListHats';
import Nav from './Nav';
import ShoesList from './ShoesManagement'
import ShoesM from './ShoeForm';


function App(props) {

  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList/>} />
              <Route path="new" element={<ShoesM />} />
          </Route>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<ListHats />} />
        </Routes>
        {/* <ShoesManagement/> */}
    </BrowserRouter>
  );
}

export default App;
