import React, { useState, useEffect } from 'react';


const ShoesList = (props) => {

    const {shoes} = props

    const deleteData = async (id) => {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}`, {method: 'DELETE'})
    }

    return <>
    <div id="shoes_list">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>manufacturer</th>
              <th>model</th>
              <th>color</th>
              <th>picture</th>
              <th>bin</th>
              <th>closet</th>
              <th>delete</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
              return (
                <tr key={shoe.href}>
                    <td>{ shoe.id }</td>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.model_name }</td>
                    <td>{ shoe.color }</td>
                    <td> <img src={ shoe.picture_url } width="200px" ></img></td>
                    <td>{ shoe.bin.import_href }</td>
                    <td>{ shoe.bin.closet_name }</td>
                    <td><button onClick={() => deleteData(shoe.id)} className="btn btn-outline-danger" >Delete</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>
    </>
}




function ShoesManagement() {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/')

        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setShoes(data.shoes)
        }

    }



    useEffect(()=>{
        getData()
      }, [])

    return (
        <>
            <ShoesList shoes={shoes}/>
        </>
      );
    }
export default ShoesManagement
