import React, {useState, useEffect } from 'react';

        const ShoesCreate = () => {
                const [bins, setBins] = useState([])
                const [formData, setFormData] = useState({
                    manufacturer: '',
                    model_name: '',
                    color: '',
                    picture_url: '',
                    bin: '',
                })

                const getData = async () => {
                    const url = 'http://localhost:8100/api/bins/';
                    const response = await fetch(url);

                    if (response.ok) {
                      const data = await response.json();
                      setBins(data.bins);
                    }
                  }

                  useEffect(()=> {
                    getData();
                  }, [])


                const handleSubmit = async (event) => {
                    event.preventDefault();

                    const shoeUrl = 'http://localhost:8080/api/shoes/';

                    const fetchConfig = {
                      method: "post",
                      body: JSON.stringify(formData),
                      headers: {
                        'Content-Type': 'application/json',
                      },
                    };

                const response = await fetch(shoeUrl, fetchConfig);
                console.log(response)
                if (response.ok) {
                    setFormData({
                        manufacturer: '',
                        model_name: '',
                        color: '',
                        picture_url: '',
                        bin: '',
                    });
                  }
                }


                const handleChangeInput = (e) => {
                    const value = e.target.value;
                    const inputName = e.target.name;

                    setFormData({
                        ...formData,
                        [inputName]: value
                    })
                }

                console.log(formData)

                return (
                    <div className="row">
                        <div className="offset-3 col-6">
                            <div id="shoes_create">
                                <h3>Create Shoes</h3>
                                    <form onSubmit={handleSubmit} id="create_shoe_form">

                                        <div className='mb-2'>
                                            <label htmlFor="manufacturer" className="form-label">manufacturer</label>
                                            <input value={formData.manufacturer} onChange={handleChangeInput} type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                        </div>

                                        <div className='mb-2'>
                                            <label htmlFor="model_name" className="form-label">model name</label>
                                            <input value={formData.model_name} onChange={handleChangeInput} type="text" name="model_name" id="model_name" className="form-control" />
                                        </div>

                                        <div className='mb-2'>
                                            <label htmlFor="color" className="form-label">color</label>
                                            <input value={formData.color} onChange={handleChangeInput} type="text" name="color" id="color" className="form-control" />
                                        </div>

                                        <div className='mb-2'>
                                            <label htmlFor="picture_url" className="form-label">picture_url</label>
                                            <input value={formData.picture_url} onChange={handleChangeInput} type="text" name="picture_url" id="picture_url" className="form-control" />
                                        </div>

                                        <div className="mb-3">
                                            <option value="">Choose a bin</option>
                                            <select value={formData.bin} onChange={handleChangeInput} required name="bin" id="bin" className="form-select">
                                            {
                                                bins.map(bin => {
                                                    return (
                                                        <option key={bin.href} value={bin.id}>{bin.closet_name}</option>
                                                    )
                                                })
                                            }

                                        </select>
                                        </div>

                                            <button className="btn btn-primary">Create</button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                    )

            }

            function ShoesM() {
                const [shoes, setShoes] = useState([])

                const getData = async () => {
                    const response = await fetch('http://localhost:8080/api/shoes/')

                    if (response.ok) {
                        const data = await response.json()
                        console.log(data)
                        setShoes(data.shoes)
                    }
                }

                useEffect(()=>{
                    getData()
                  }, [])

                return (
                    <>
                        <ShoesCreate shoes={shoes}/>
                    </>
                  );
                }
            export default ShoesM














































//     const [bins, setBins] = useState([])

//     const [formData, setFormData] = useState({
//         manufacturer: '',
//         model_name: '',
//         color: '',
//         picture_url: '',
//         bin: '',
//       })

//       const [hasSignedUp, setHasSignedUp] = useState(false)

//       const getData = async () => {
//         const url = 'http://localhost:8100/api/bins/';
//         const response = await fetch(url);
//         console.log(response)
//         if (response.ok) {
//           const data = await response.json();
//           setBins(data.bins);
//         }
//       }

//       useEffect(() => {
//         getData();
//       }, []);

//       const handleSubmit = async (event) => {
//         event.preventDefault();

        // const binUrl = `http://localhost:8080/api/shoes/`;

        // const fetchConfig = {
        //   method: "post",
        //   body: JSON.stringify(formData),
        //   headers: {
        //     'Content-Type': 'application/json',
        //   },
        // };

        // const response = await fetch(binUrl, fetchConfig);

        // if (response.ok) {
        //     setFormData({
        //         manufacturer: '',
        //         model_name: '',
        //         color: '',
        //         picture_url: '',
        //         bin: '',
        //     });

        //     setHasSignedUp(true);
        //   }
        // }

        // const handleChangeName = (e) => {
        //     const value = e.target.value;
        //     const inputName = e.target.name;
        //     setFormData({
        //       ...formData,
        //       [inputName]: value
        //     });
        //   }

        //   const formClasses = (!hasSignedUp) ? '' : 'd-none';
        //   const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

//     return (
//         <div className="row">
//             <div className="offset-3 col-6">
//                 <div id="shoes_create">
//                     <h3>Create Shoes</h3>
//                         <form className={formClasses} onSubmit={handleSubmit} id="create_shoe_form">

//                             <div className='mb-2'>
//                                 <label htmlFor="manufacturer" className="form-label">manufacturer</label>
//                                 <input type="text" name="manufacturer" id="manufacturer" className="form-control" />
//                             </div>

//                             <div className='mb-2'>
//                                 <label htmlFor="model_name" className="form-label">model name</label>
//                                 <input onChange={handleChangeName} type="text" name="model_name" id="model_name" className="form-control" />
//                             </div>

//                             <div className='mb-2'>
//                                 <label htmlFor="color" className="form-label">color</label>
//                                 <input onChange={handleChangeName} type="text" name="color" id="color" className="form-control" />
//                             </div>

//                             <div className='mb-2'>
//                                 <label htmlFor="picture_url" className="form-label">picture_url</label>
//                                 <input onChange={handleChangeName} type="url" name="picture_url" id="picture_url" className="form-control" />
//                             </div>

//                             <div className="mb-3">
//                                 <option value="">Choose a bin</option>
//                                 <select onChange={handleChangeName} required name="bin" id="bin" className="form-select">
//                                 <option value="">Choose a Bin</option>
//                     {
//                       bins.map(bin => {
//                         return (
//                           <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
//                         )
//                       })
//                     }

//                                 </select>
//                             </div>
//                             <button className="btn btn-primary">Create</button>
//                         </form>
//                         <div className={messageClasses} id="success-message">
//                 Congratulations! You're all signed up!
//               </div>

//                 </div>
//             </div>
//         </div>
//     )
// }

// export default ShoeForm
